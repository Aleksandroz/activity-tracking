class RecordsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :there_are_categories
  
  def index
    @records = Record.all
  end

  def show
    @record = Record.find(params[:id])
    @category = Category.find(@record.category_id)
  end

  def new
    @record = Record.new
    @categories = Category.all
  end

  def create
    @record = Record.new(params[:record])

    if @record.save
      redirect_to @record, :notice => "Successfully created record."
    else
      render :action => 'new'
    end
  end

  def edit
    @record = Record.find(params[:id])
    @categories = Category.all
  end

  def update
    @record = Record.find(params[:id])
    if @record.update_attributes(params[:record])
      redirect_to @record, :notice  => "Successfully updated record."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @record = Record.find(params[:id])
    @record.destroy
    redirect_to category_path(@record.category_id), :notice => "Successfully destroyed record."
  end
  
  private
    def there_are_categories
        if Category.all.count == 0 then
            redirect_to user_path(current_user), :notice => "There are no categories. You first need to create categories."
        end
    end
end
