class CategoriesController < ApplicationController
  before_filter :authenticate_user!

  def index
    @categories = Category.all
  end

  def show
    @category = Category.find(params[:id])
    #@records = Record.records_from_category(params[:id])
    #@records = Record.records_from_category(params[:id]).paginate(:per_page => 5, :page => params[:page])
    @search_results = Record.records_from_category(params[:id]).search(params[:search])
    @records = @search_results.paginate(:per_page => 20, :page => params[:page])
    if !params[:search].nil?
        @number_of_search_results = @search_results.count
    end
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(params[:category])
    if @category.save
      redirect_to current_user, :notice => "Successfully created category."
    else
      render :action => 'new'
    end
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])
    if @category.update_attributes(params[:category])
      redirect_to @category, :notice  => "Successfully updated category."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @category = Category.find(params[:id])
    @category.destroy
    redirect_to user_path(current_user), :notice => "Successfully destroyed category."
  end
end
