class UsersController < ApplicationController

  def show

    @user_categories = Category.user_categories(params[:id]).paginate(:per_page => 80, :page => params[:page])
    
  end

end
