class Category < ActiveRecord::Base
  has_one :user
  has_many :records
  
  attr_accessible :user_id, :name
  
  scope :user_categories, lambda { |entity| where("user_id >= ?", entity)}
  
end
