class Record < ActiveRecord::Base
  has_one :category
  
  acts_as_taggable_on :tags
  
  attr_accessible :category_id, :record_date, :information,:tag_list
  
  scope :records_from_category, lambda { |entity| where("category_id >= ?", entity)}
  
  def self.search(search)
    if search
        find(:all, :conditions => ['information LIKE ?', "%#{search}%"])
      else
        find(:all)
      end
end
  
end
