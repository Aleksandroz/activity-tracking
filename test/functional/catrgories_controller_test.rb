require 'test_helper'

class CatrgoriesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => Catrgory.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    Catrgory.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    Catrgory.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to catrgory_url(assigns(:catrgory))
  end

  def test_edit
    get :edit, :id => Catrgory.first
    assert_template 'edit'
  end

  def test_update_invalid
    Catrgory.any_instance.stubs(:valid?).returns(false)
    put :update, :id => Catrgory.first
    assert_template 'edit'
  end

  def test_update_valid
    Catrgory.any_instance.stubs(:valid?).returns(true)
    put :update, :id => Catrgory.first
    assert_redirected_to catrgory_url(assigns(:catrgory))
  end

  def test_destroy
    catrgory = Catrgory.first
    delete :destroy, :id => catrgory
    assert_redirected_to catrgories_url
    assert !Catrgory.exists?(catrgory.id)
  end
end
